import Vue from 'vue'
import App from './App.vue'
import axios from '@/axios'


import router from '@/router'
// Vue.use(axios) 因为axios 不是 vue的插件 不能用use 方法

// vue 原型链 增加一个 axios属性  this.axios
Vue.prototype.axios = axios

Vue.config.productionTip = false

new Vue({
	router,
	render: h => h(App),
}).$mount('#app')
