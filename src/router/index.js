import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// 解决 vue 重复点击一个路由 报错
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
	return originalPush.call(this, location).catch(err => err)
}
// 定义路由规则
const routes = [{
		name: 'index',
		path: '/',
		component: () => import("@/pages/index")
	},
	{
		name: 'list',
		path: '/list',
		component: () => import("@/pages/list")
	},
	{
		name: 'info',
		path: '/info',
		component: () => import("@/pages/info")
	}
]

// 实例化vuerouter

const router = new VueRouter({
	routes
})

export default router
